## Easy Streaming Images Upload

Using `streams` to upload file, optimize image from two main extension `jpeg` or `png`

Version 0.2.x: Optimize file specific type `jpeg` or `png` following and save to different folder

------

**Install**

```
npm install --save easy-stream-upload
```