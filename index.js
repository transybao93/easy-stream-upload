"use strict"

//- create file by projects
function folderAction(folderPath)
{
    let fs = require('fs')
    let isPathExist = true
    let samplePath = 'public/'

    //- check if path is exist
    if (!fs.existsSync(folderPath)){
        //- if not exist
        fs.mkdirSync(folderPath);
    }else{
        //- if path exist
        console.log('folder exist')
    }
    return true;
}

function simpleValidation(imgName, imgAbsolutePath, saveTo)
{
    let returned_data = {}
    
    //- validate some variables
    if(typeof imgName !== 'string' ||  typeof imgAbsolutePath !== 'string' || typeof saveTo !== 'string')
    {
        return false;
    }

    //- analyze some variables
    returned_data.imgName         = imgName.trim()
    returned_data.imgAbsolutePath = imgAbsolutePath.trim()
    returned_data.saveTo          = saveTo.trim()
    //- return
    return returned_data;
}

function getExtensionFromPath(imgPath)
{
    let fext = require('file-extension')
    let extention = fext(imgPath).toLowerCase()
    return extension
}

function gulpOptimize(saveTo)
{
    //- optimize current folder using gulp
    const gulp = require('gulp');
    const imagemin = require('gulp-imagemin');

    gulp.task('default', () =>
        gulp.src(saveTo + '/')
            .pipe(imagemin())
            .pipe(gulp.dest(saveTo))
    );
}

function saveToS3()
{
    
}

function imgOptimize(imgAbsolutePath, saveTo, imgName)
{
    //- get extension
    let extention = getExtensionFromPath(imgAbsolutePath)
    
    //- using sharp
    let sharp = require('sharp')

    //- sharp configuration
    sharp.cache(true)
    sharp.concurrency(0)
    sharp.simd(true)

    //- filter by extension
    if(extention === 'jpg' || extention === 'jpeg')
    {
        sharp(imgAbsolutePath)
        .flatten()
        .normalise(true)
        .crop(sharp.strategy.attention) //- focus on people
        .jpeg({
            quality: 50, 
            progressive: true,
            optimiseScans: true,
            chromaSubsampling: '4:4:4'
        })//- if jpeg
        .toFile(saveTo + '/' + imgName + '.' + extention, function(err, info){
            if(err){
                console.log(err)
            }else{
                console.log(info)
            }
        })
    }

    if(extention === 'png')
    {
        sharp(imgAbsolutePath)
        .flatten()
        .normalise(true)
        .crop(sharp.strategy.entropy) //- focus on people
        .png({
            progressive: true,
            adaptiveFiltering: true,
            compressionLevel: 8
        })
        .toFile(saveTo + '/' + imgName + '.' + extention, function(err, info){
            if(err){
                console.log(err)
            }else{
                console.log(info)
            }
        })
    }

}

module.exports = {

    uploadSingle(imgName="", imgAbsolutePath="", saveTo=".tmp/public/", callback){

        let validated_data = simpleValidation(imgName, imgAbsolutePath, saveTo)
        if(typeof validated_data === 'object')
        {
           
            console.log('validated_data', validated_data)
            //- some variables will be here
            
            if(folderAction(validated_data.saveTo))
            {
                //- if get through create folder stuffs
                //- then get file from path then optimize image then save it to specific folder
                //- img optimize
                imgOptimize(validated_data.imgAbsolutePath, validated_data.saveTo, validated_data.imgName)
            }
        
            //- change the image name to <CurrentDateTimeInMilliseconds_uniqueID>
            //- optimize image sizes
        
            return new Promise((resolve, reject) => {
                //- async works is here
                //- uploading image in async way
                
        
            })

        }else{
            //- callback errors
            return callback('Data is not in appropriate types. Please check again !!!')
        }
    
        
    
    },
    
}